from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve


# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
  def test_diplomacy_1(self):
    input = StringIO('A Madrid Hold')
    expected_output = 'A Madrid\n'
    w = StringIO()
    diplomacy_solve(input, w)
    self.assertEqual(w.getvalue(), expected_output)

  def test_diplomacy_2(self):
    input = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London')
    expected_output = 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n'
    w = StringIO()
    diplomacy_solve(input, w)
    self.assertEqual(w.getvalue(), expected_output)

  def test_diplomacy_3(self):
    input = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid')
    expected_output = 'A [dead]\nB [dead]\nC [dead]\n'
    w = StringIO()
    diplomacy_solve(input, w)
    self.assertEqual(w.getvalue(), expected_output)

  def test_diplomacy_4(self):
    input = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B')
    expected_output = 'A [dead]\nB Madrid\nC [dead]\nD Paris\n'
    w = StringIO()
    diplomacy_solve(input, w)
    self.assertEqual(w.getvalue(), expected_output)

  def test_diplomacy_5(self):
    input = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A')
    expected_output = 'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n'
    w = StringIO()
    diplomacy_solve(input, w)
    self.assertEqual(w.getvalue(), expected_output)

  def test_diplomacy_6(self):
      input = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B')
      expected_output = 'A [dead]\nB Madrid\nC London\n'
      w = StringIO()
      diplomacy_solve(input, w)
      self.assertEqual(w.getvalue(), expected_output)

  def test_diplomacy_7(self):
      input = StringIO('A Madrid Hold\nB Barcelona Move Madrid\n')
      expected_output = 'A [dead]\nB [dead]\n'
      w = StringIO()
      diplomacy_solve(input, w)
      self.assertEqual(w.getvalue(), expected_output)

  def test_diplomacy_8(self):
      input = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Paris Support B\nE Austin Support A')
      expected_output = 'A [dead]\nB Madrid\nC London\nD Paris\nE Austin\n'
      w = StringIO()
      diplomacy_solve(input, w)
      self.assertEqual(w.getvalue(), expected_output)

  def test_diplomacy_9(self):
      input = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Paris Support B\nE Austin Support A\nF NYC Support A')
      expected_output = 'A [dead]\nB [dead]\nC London\nD Paris\nE Austin\nF NYC\n'
      w = StringIO()
      diplomacy_solve(input, w)
      self.assertEqual(w.getvalue(), expected_output)

  def test_diplomacy_10(self):
    input = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B \nD Paris Support B\nE Austin Support A')
    expected_output = 'A [dead]\nB Madrid\nC London\nD Paris\nE Austin\n'
    w = StringIO()
    diplomacy_solve(input, w)
    self.assertEqual(w.getvalue(), expected_output)

  def test_diplomacy_11(self):
    input = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B \nD Paris Support B\nE Austin Move London\nF Chicago Move Paris\nG Seattle Support A\nH Atlanta Support F')
    expected_output = 'A Madrid\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF Paris\nG Seattle\nH Atlanta\n'
    w = StringIO()
    diplomacy_solve(input, w)
    self.assertEqual(w.getvalue(), expected_output)

  def test_diplomacy_12(self):
      input = StringIO('A Madrid Support B\nB Barcelona Hold\nC London Move Barcelona')
      expected_output = 'A Madrid\nB Barcelona\nC [dead]\n'
      w = StringIO()
      diplomacy_solve(input, w)
      self.assertEqual(w.getvalue(), expected_output)
# ----
# main
# ----

if __name__ == "__main__":
    main()